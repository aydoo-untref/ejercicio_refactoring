Template Processor Refactoring Exercise
=======================================

Exercise based on Java Refactoring Workbook: https://github.com/lynnlangit/Java-Refactoring-Workbook/blob/master/ch6-template/Template.java

Proposed flow:

1. Identify possible refactorings and create a backlog with them
2. Add tests to ensure/discover the current behaviour
2. Apply refactorings one by one