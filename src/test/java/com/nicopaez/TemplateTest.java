package com.nicopaez;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TemplateTest {

    private Template template = new Template();
    private String altcode = "altcode";

    @Test(expected=StringIndexOutOfBoundsException.class)
    public void shouldRaiseExceptionWhenTemplateNotIncludeCODE() {
        String reqId = "reqid";
        String sourceTemplate = "Some test template";
        template.applyTemplate(sourceTemplate, reqId, altcode);
    }

    @Test(expected=StringIndexOutOfBoundsException.class)
    public void shouldRaiseExceptionWhenTemplateNotIncludeALTCODE() {
        String reqId = "1234";
        String sourceTemplate = "Some test template with %CODE% and %ALTCODE%";
        template.applyTemplate(sourceTemplate, reqId, altcode);
    }

    @Test(expected=StringIndexOutOfBoundsException.class)
    public void shouldRaiseExceptionWhenReqIdIsShorterThan8() {
        String reqId = "1234567";
        String sourceTemplate = "Some test template with %CODE% and %ALTCODE%";
        template.applyTemplate(sourceTemplate, reqId, altcode);
    }

    @Test()
    public void shouldPerformReplacement() {
        String reqId = "12345678";
        String sourceTemplate = "Some test template with %CODE% and %ALTCODE%";
        String result = template.applyTemplate(sourceTemplate, reqId, altcode);
        assertEquals("Some test template with 12345678 and 12345-678", result);
    }
}
