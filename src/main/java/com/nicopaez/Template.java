package com.nicopaez;

/**
 * Based on Java Refactoring Workbook:
 * https://github.com/lynnlangit/Java-Refactoring-Workbook/blob/master/ch6-template/Template.java
 */

public class Template {

    public static final String PLACEHOLDER1 = "%CODE%";
    public static final String PLACEHOLDER2 = "%ALTCODE%";

    @Deprecated
    public String applyTemplate(String template, String reqId, String altcode) {
        return applyTemplate(template, reqId);
    }

    public String applyTemplate(String template, String reqId) {
        String altcode;
        String code;

        int templateSplitBegin = template.indexOf(PLACEHOLDER1);
        int templateSplitEnd = templateSplitBegin + PLACEHOLDER1.length();
        String templatePartOne = new String(template.substring(0, templateSplitBegin));
        String templatePartTwo = new String(template.substring(templateSplitEnd, template.length()));
        code = new String(reqId);
        template = new String(templatePartOne + code + templatePartTwo);

        templateSplitBegin = template.indexOf(PLACEHOLDER2);
        templateSplitEnd = templateSplitBegin + PLACEHOLDER2.length();
        templatePartOne = new String(template.substring(0, templateSplitBegin));
        templatePartTwo = new String(template.substring(templateSplitEnd, template.length()));
        altcode = code.substring(0, 5) + "-" + code.substring(5, 8);
        return templatePartOne + altcode + templatePartTwo;
    }
}